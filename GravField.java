public class GravField
{   
	/**
	*Stores the acceleration effects due to gravitational fields on the particle
	*that this GravField is a part of.
	*/
	PhysicsVector gravField;
	PhysicsVector radius;
	PhysicsVector midGravField;
	double gEnergy;
	static double G = 6.67408*Math.pow(10,-11);       
	
    public GravField()
    {    	
    	this.gravField = new PhysicsVector();
        this.radius = new PhysicsVector();
        this.midGravField = new PhysicsVector();
    }
    
    /**
    *This is the function that actually calculates the acceleration due to the 
    *gravitational field of another particle.
    *It calculates the vector distance between them and uses that to find the 
    *field direction and strength due to that particle.
    *@param thisParticle The particle that the effects are being calculated for
    *@param otherParticle The particle that the effects are being calculated due to
    *@param mass The mass of thisParticle
    *@param step The step length between calculations, for scaling the acceleration
    *due to v=u+at, this is the t
    */
    public void updateGravField(PhysicsVector thisParticle, PhysicsVector otherParticle, double mass, double step)
    {    
    	PhysicsVector temp = new PhysicsVector(otherParticle);
    	temp.decreaseBy(thisParticle);
    	radius.setVector(temp);
        double fGrav = (step*G*mass)/(Math.pow(radius.magnitude(),3)); 
        gravField.setVector(radius.scale(fGrav,radius));
    }
    
    /**
    *This is the function that actually calculates the acceleration due to the 
    *gravitational field of another particle.
    *It calculates the vector distance between them and uses that to find the 
    *field direction and strength due to that particle.
    *Exactly the same as updateGravField, but for the midpoint.
    *@param thisParticle The particle that the effects are being calculated for
    *@param otherParticle The particle that the effects are being calculated due to
    *@param mass The mass of thisParticle
    *@param step The step length between calculations, for scaling the acceleration
    *due to v=u+at, this is the t
    */
    public void updateMidGravField(PhysicsVector thisParticle, PhysicsVector otherParticle, double mass, double step)
    {    
    	PhysicsVector temp = new PhysicsVector(otherParticle);
    	temp.decreaseBy(thisParticle);
    	radius.setVector(temp);
        double fGrav = (G*mass)/(Math.pow(radius.magnitude(),3)); 
        midGravField.setVector(radius.scale(fGrav,radius)); 
    }
    
    /**
    *Calculates the potential energy due to the gravitational field of one 
    *particle, to be summed in a loop in the main method for all particles in a 
    *time slice.
    *@param step The length of simulated time between particles
    *@param mass The mass of the particle the energy is calculated for
    *@return A scalar containing the gravitational potential energy of the particle
    */
    public double particleGravitationalPotentialEnergy(double step, double mass)
    {
    	double radMag = radius.magnitude();
    	gEnergy = gravField.magnitude()*mass*radMag/step;
    	return gEnergy;
    }
    
    //Simply just finds the cube of the radius
    /*public static double radiusCubed(double xDist,double yDist)
    {
    	double RCubed = Math.pow(Math.pow(xDist,2)+ Math.pow(yDist+rEarth,2),1.5);
    	return RCubed;
    }*/
}