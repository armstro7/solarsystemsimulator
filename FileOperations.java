import java.util.*;
import java.io.*;
import java.text.*;
import java.util.concurrent.TimeUnit;
 
/**
*Contains all of the different methods that print out the files that contain the
*data from the program. There is a different method for each variable that is to
*be printed, which may not be the best way but it works and makes it very easy 
*to input into a graphing program like QTIplot.
*This also contains the createDirectory method that produces the folders for the
*data to be stored in, and starts off with one based on the current time so that
*no data will ever be overwritten.
*/

public class FileOperations
{
	/**
	*Prints the x position of a particle to its own file
	*@param particles The list of all particles in the simulation
	*@param timeStamp The String formatted start time of the simulation, for 
	*finding the right folder to print to.
	*/
    public static void printxPositionToFile(ArrayList<Particle> particles,String timeStamp) throws IOException
    {
    	
        String particleFolderName;
    	for(int i=0;i<particles.size();i++)
        {
        	particleFolderName = particles.get(i).name;
        	File xPositionFile = new File(timeStamp + File.separator + particleFolderName + File.separator + "xPosition.txt" );
        	FileWriter p = new FileWriter(xPositionFile,true);
        	String value = Double.toString(particles.get(i).position.getX());
        	p.write(value + "\r\n");
        	p.close();
        }    	
    }
    
    /**
	*Prints the y position of a particle to its own file
	*@param particles The list of all particles in the simulation
	*@param timeStamp The String formatted start time of the simulation, for 
	*finding the right folder to print to.
	*/
    public static void printyPositionToFile(ArrayList<Particle> particles,String timeStamp) throws IOException
    {
    	
        String particleFolderName;
    	for(int i=0;i<particles.size();i++)
        {
        	particleFolderName = particles.get(i).name;
        	File PositionFile = new File(timeStamp + File.separator + particleFolderName + File.separator + "yPosition.txt" );
        	FileWriter p = new FileWriter(PositionFile,true);
        	String value = Double.toString(particles.get(i).position.getY());
        	p.write(value + "\r\n");
        	p.close();
        }    	
    }
    
    /**
	*Prints the z position of a particle to its own file
	*@param particles The list of all particles in the simulation
	*@param timeStamp The String formatted start time of the simulation, for 
	*finding the right folder to print to.
	*/
    public static void printzPositionToFile(ArrayList<Particle> particles,String timeStamp) throws IOException
    {
    	
        String particleFolderName;
    	for(int i=0;i<particles.size();i++)
        {
        	particleFolderName = particles.get(i).name;
        	File PositionFile = new File(timeStamp + File.separator + particleFolderName + File.separator + "zPosition.txt" );
        	FileWriter p = new FileWriter(PositionFile,true);
        	String value = Double.toString(particles.get(i).position.getZ());
        	p.write(value + "\r\n");
        	p.close();
        }    	
    }
    
    /**
	*Prints the x velocity of a particle to its own file
	*@param particles The list of all particles in the simulation
	*@param timeStamp The String formatted start time of the simulation, for 
	*finding the right folder to print to.
	*/
    public static void printxVelocityToFile(ArrayList<Particle> particles,String timeStamp) throws IOException
    {    	
        String particleFolderName;
    	for(int i=0;i<particles.size();i++)
        {
        	particleFolderName = particles.get(i).name;
        	File PositionFile = new File(timeStamp + File.separator + particleFolderName + File.separator + "xVelocity.txt" );
        	FileWriter p = new FileWriter(PositionFile,true);
        	String value = Double.toString(particles.get(i).velocity.getX());
        	p.write(value + "\r\n");
        	p.close();
        }    	
    }
    
    /**
	*Prints the y velocity of a particle to its own file
	*@param particles The list of all particles in the simulation
	*@param timeStamp The String formatted start time of the simulation, for 
	*finding the right folder to print to.
	*/
    public static void printyVelocityToFile(ArrayList<Particle> particles,String timeStamp) throws IOException
    {    	
        String particleFolderName;
    	for(int i=0;i<particles.size();i++)
        {
        	particleFolderName = particles.get(i).name;
        	File PositionFile = new File(timeStamp + File.separator + particleFolderName + File.separator + "yVelocity.txt" );
        	FileWriter p = new FileWriter(PositionFile,true);
        	String value = Double.toString(particles.get(i).velocity.getY());
        	p.write(value + "\r\n");
        	p.close();
        }    	
    }
    
    /**
	*Prints the z velocity of a particle to its own file
	*@param particles The list of all particles in the simulation
	*@param timeStamp The String formatted start time of the simulation, for 
	*finding the right folder to print to.
	*/
    public static void printzVelocityToFile(ArrayList<Particle> particles,String timeStamp) throws IOException
    {    	
        String particleFolderName;
    	for(int i=0;i<particles.size();i++)
        {
        	particleFolderName = particles.get(i).name;
        	File PositionFile = new File(timeStamp + File.separator + particleFolderName + File.separator + "zVelocity.txt" );
        	FileWriter p = new FileWriter(PositionFile,true);
        	String value = Double.toString(particles.get(i).velocity.getZ());
        	p.write(value + "\r\n");
        	p.close();
        }    	
    }
    
    /**
	*Prints the energy of a particle to its own file
	*@param particles The list of all particles in the simulation
	*@param timeStamp The String formatted start time of the simulation, for 
	*finding the right folder to print to.
	*/
    public static void printEnergyToFile(ArrayList<Particle> particles,String timeStamp) throws IOException
    {    	
        String particleFolderName;
    	for(int i=0;i<particles.size();i++)
        {
        	particleFolderName = particles.get(i).name;
        	File PositionFile = new File(timeStamp + File.separator + particleFolderName + File.separator + "Energy.txt" );
        	FileWriter p = new FileWriter(PositionFile,true);
        	String value = Double.toString(particles.get(i).energy);
        	p.write(value + "\r\n");
        	p.close();
        }    	
    }
    
    /**
	*Prints the x velocity of a particle to its own file
	*@param particles The list of all particles in the simulation
	*@param timeStamp The String formatted start time of the simulation, for 
	*finding the right folder to print to.	
	*@param totalEnergy The value of the sum of all particles' energies
	*/    
    public static void printTotalEnergyToFile(ArrayList<Particle> particles,String timeStamp, double totalEnergy) throws IOException
    {    	
        
        File PositionFile = new File(timeStamp + File.separator + "totalEnergy.txt" );
        FileWriter p = new FileWriter(PositionFile,true);
        String value = Double.toString(totalEnergy);
        p.write(value + "\r\n");
        p.close();
    }
    
    /**
    *Prints out some information about the simulation, the amount of energy lost
    *during the simulation at the top of the file, followed by on separate lines,
    *the total energy at the start of the simulation and the energy lost as a 
    *percentage of this, the conditions set on step lengths, and finally how 
    *long it took for the program to run.
    *@param timeStamp The String formatted start time of the simulation, for 
	*finding the right folder to print to.
	*@param energyDifference The initial energy - the final energy, to see how 
	*much the system loses during its run due to inaccuracies.
	*@param startTime The Date that the program started at, for calculating how
	*long the program ran for.
	*@param totalEnergy The total energy in the particle system.
	*@param stepLength The length of simulated time between particles.
	*@param simulationLength The total time the simulation simulates in seconds.
	*@param printStep The number of steps between values are printed out.
    */
    public static void printDiagnostics(String timeStamp, double energyDifference, Date startTime, double totalEnergy, double stepLength, double simulationLength, double printStep) throws IOException
    {    
        File PositionFile = new File(timeStamp + File.separator + "Diagnostics.txt" );
        FileWriter p = new FileWriter(PositionFile,true);
        String value = "The simulation lost " + Double.toString(energyDifference) + " Joules of energy while running.";
        p.write(value + "\r\n");
        p.write("This is compared to the total energy of " + totalEnergy + " Joules, corresponding to a loss of " + 100*energyDifference/totalEnergy + "%. \r\n");
        p.write("The simulation calculated a time " + simulationLength + " seconds from its start point with a step length of " + stepLength + " seconds and printed out values every " + printStep + " steps. \r\n");
        p.write(calculateTimeDifference(startTime) + "\r\n");
        p.close();
    }
    
    /**
    *Calculates how long the simulation ran for. This will mostly depend on the
    *number of outputs to files there are as that is the most time consuming.
    */
    public static String calculateTimeDifference(Date startTime)
    {
    	Date endDate   = Calendar.getInstance().getTime();

    	long duration  = endDate.getTime() - startTime.getTime();

    	long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
    	long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
    	long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
    	
    	diffInMinutes -= diffInHours*60;
    	diffInSeconds -= diffInMinutes*60;
    	String timeDifference = ("Your simulation took " + diffInHours + " hours, " + diffInMinutes + " minutes and " + diffInSeconds + " seconds to compute");
    	return timeDifference;
    }
    
    public static void createDirectory(ArrayList<Particle> particles, String timeStamp)
    {
        File dateFolder = new File(timeStamp);
        dateFolder.mkdir();
        String particleFolderName;
        for(int i=0;i<particles.size();i++)
        {
        	particleFolderName = particles.get(i).name;
        	File particleFolder = new File(timeStamp + File.separator + particleFolderName);
        	particleFolder.mkdir();
        }
    }
    
    public static void printEverything(ArrayList<Particle> particles, String timeStamp, double totalEnergy) throws IOException
    {
    	printxPositionToFile(particles,timeStamp);
        printyPositionToFile(particles,timeStamp);
        printzPositionToFile(particles,timeStamp);
        printxVelocityToFile(particles,timeStamp);
        printyVelocityToFile(particles,timeStamp);
        printzVelocityToFile(particles,timeStamp);
        printEnergyToFile(particles,timeStamp);
        printTotalEnergyToFile(particles,timeStamp,totalEnergy);
    }
}