import java.util.*;
import java.io.*;
import java.text.*;
 
public class Particle
{
	/**
	*This is the particle class. It is responsible for creating particle objects
	*and holding the data relevant to each one. Each PhysicsVector is a 3D vector
	*object that holds an x, y and z coordinate.
	*The position object contains the position of the particle
	*The velocity object contains the velocity of the particle
	*The gravField contains the direction and strength of the force due to gravity
	*that the particle experiences multiplied by the step length, resulting in 
	*the change in velocity.
	*The charge, mass and name of the particle are also created and stored here.
	*@param xVel The x velocity of the particle in m/s
	*@param yVel The y velocity of the particle in m/s
	*@param zVel The z velocity of the particle in m/s
	*@param xPos The x position of the particle in m
	*@param yPos The y position of the particle in m
	*@param zPos The z position of the particle in m
	*@param pMass The mass of the particle in kg
	*@param pName The name of the particle. ie. Earth
	*/
	PhysicsVector position;
	PhysicsVector velocity;
	GravField gravField;
	PhysicsVector midVelocity;
	PhysicsVector midPosition;
	double charge;
	double mass;
	String name;
	double energy;
	double kEnergy;
	double gEnergy;
	
    public Particle(double xVel, double yVel, double zVel, double xPos, double yPos, double zPos, double pMass, String pName)
    {
    	//Creates 3D vectors for position and velocity
    	
        this.position = new PhysicsVector(xPos, yPos, zPos);
        this.velocity = new PhysicsVector(xVel, yVel, zVel);
        this.midPosition = new PhysicsVector();
        this.midVelocity = new PhysicsVector();
        this.mass = pMass;
        this.charge = 0;  
        this.name = pName;
        this.gravField = new GravField();
        this.energy = 0;
        this.kEnergy = 0;
        this.gEnergy = 0;
    }
    
    /**
    *Here is the simple method that adds the velocity with the change in velocity
    *produced by the gravField. This method is used before updating the position 
    *as per the Euler-Cramer method, but used by the midpoint as per the Euler-
    *Richardson method.
    *@param step The length of simulated time between particles
    *@param fGrav The gravitational force due to the other particles 
    */
    public void updateRichardsonVelocity(GravField fGrav, double step)
    {
    	velocity.setVector(velocity.add(velocity,fGrav.midGravField.scale(step,fGrav.midGravField)));
    }
    
    /**
    *Here is the simple method that adds the velocity with the change in velocity
    *produced by the gravField. This method is used before updating the position 
    *as per the Euler-Cramer method.
    *@param step The length of simulated time between particles
    *@param fGrav The gravitational force due to the other particles 
    */
    public void updateVelocity(GravField fGrav, double step)
    {
    	velocity.setVector(velocity.add(velocity,fGrav.gravField));
    }
    
    /**
    *Here is the simple method that adds the velocity with the change in velocity
    *produced by the gravField. This method is used before updating the position 
    *as per the Euler-Cramer method, but finds the middle point velocity.
    *@param step The length of simulated time between particles
    *@param fGrav The gravitational force due to the other particles 
    */
    public void updateMidVelocity(GravField fGrav, double step)
    {
    	midVelocity.setVector(velocity.add(velocity,fGrav.gravField.scale(step,fGrav.gravField)));
    }
    
    /**
    *This method adds the position with velocity multiplied by the time step.
    *Approximating it to a series of small, straight lines.
    *This uses the midpoint of the velocity for the Euler-Richardson method 
    *instead of the start velocity.
    *@param step The length of simulated time between particles
    */
    public void updateRichardsonPosition(double step)
    {
    	 position.setVector(position.add(position,midVelocity.scale(step,midVelocity)));
    }
    
    /**
    *This method adds the position with velocity multiplied by the time step.
    *Approximating it to a series of small, straight lines.
    *@param step The length of simulated time between particles
    */
    public void updatePosition(double step)
    {
    	 position.setVector(position.add(position,velocity.scale(step,velocity)));
    }
    
    /**
    *This method adds the position with velocity multiplied by the time step.
    *Approximating it to a series of small, straight lines. This finds the 
    *midpoint for the Euler-Richardson method.
    *@param step The length of simulated time between particles
    */
    public void updateMidPosition(double step)
    {
    	 midPosition.setVector(position.add(position,velocity.scale(step,velocity)));
    }
    
    /**
    *Calculates the kinetic energy of the particle
    */
    public void particleKineticEnergy()
    {
    	double velMag = velocity.magnitude();
    	kEnergy = 0.5*mass*velMag*velMag;
    }
    
    /**
    *Calculates the total energy of the particle
    */
    public void particleEnergy()
    {
    	particleKineticEnergy();
    	energy = kEnergy + gEnergy;
    }
    
    /**
    *Populates the particle array with existing values for our Solar System, 
    *taken directly from NASA's website.
    *@param particles A list containing all of the particles in the simulation
    */
    public static void SolarSystem(ArrayList<Particle> particles)
    {
    	double[] xPositions = {5.287798505068179E+8,-4.922500818593647E+10,6.968204586082295E+10,1.195346329901131E+11,1.898963843868802E+11,-8.107203157430899E+11,-3.276261118812044E+11,2.758638167310884E+12,4.230056828829524E+12};
        double[] yPositions = {5.253663604443358E+08,-4.364620359186402E+10,-8.349085474647081E+10,8.944406397614631E+10,-8.191082378145216E+10,-8.391255834204261E+10,-1.465110968576673E+12,1.138924291537055E+12,-1.477663990142212E+12};
        double[] zPositions = {-2.381055879763173E+07,9.313446720231641E+08,-5.166395022959769E+09,-2.806032846913114E+7,-6.398796842447761E+9,1.847954812550345E+10,3.851337854676920E+10,-3.150869168649930E+10,-6.705629583964419E+10};
        double[] xVelocities = {-3.674954798414027,2.239976797217730E+04,2.680515632412617E+4,-1.832476771836777E+04,1.059110870201336E+04,1.193389141739529E+3,8.896080354953112E+3,-2.648476950574710E+3,1.756188215340511E+3};
        double[] yVelocities = {1.177452698406634E+1,-3.421972617353248E+04,2.214267401765010E+04,2.377464330288488E+04,2.430196019366710E+04,-1.237702199012249E+4,-2.138997207095388E+3,5.977163018622440E+3,5.162792889613542E+3};
        double[] zVelocities = {7.586087036821811E-02,-4.852382775114613E+3,-1.243509536628279E+3,-1.447336365101748,2.490727920550384E+2,2.478964669651518E+1,-3.164588407401737E+2,5.656969855016802E+1,-1.473811923378519E+2};
        String[] planetNames = {"Sun","Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uranus","Neptune"};
        double[] planetMasses = {1.988544e30,3.302e23,48.685e23,5.97219e24,6.4185e23,1898.13e24,5.68319e26,86.8103e24,102.21e24};
        
        for(int i = 0;i<xPositions.length;i++)
        {
        	particles.add(new Particle(xVelocities[i],yVelocities[i],zVelocities[i],xPositions[i],yPositions[i],zPositions[i],planetMasses[i],planetNames[i]));
        }
    }    
    
    /**
    *Allows the user to input values for each parameter of a custom system of 
    *particles. There is currently no option to only input partial particles,
    *but for the purposes of this simulation I doubt this will be an issue.
    *@param particles A list containing all of the particles in the simulation
    */
    public static void CustomSystem(ArrayList<Particle> particles)
    {
    	double xPosition = 0;
        double yPosition = 0;
        double zPosition = 0;
        double xVelocity = 0;
        double yVelocity = 0;
        double zVelocity = 0;
        String Name = "";
        double Mass = 0;
        String choice = "";
        Scanner scanner = new Scanner(System.in); 
        boolean pass = false;
        
        while(choice.equals("N")==false)
        {
        	if(pass == true)
        	{
        		choice = scanner.nextLine(); //To eat the newline character nextDouble leaves
        	}
        	System.out.println("Press any key to create a new particle, or N to exit and start the simulation.");
        	choice = scanner.nextLine();
        	if(choice.equals("N"))
        	{
        		if(particles.size()>=1)
        		{
        		break;
        		}
        		else
        		{
        			System.out.println("You must enter at least one particle.");
        			choice = "";
        		}
        	}   	
        	System.out.println("Enter your particle name");
        	Name = scanner.nextLine();
        	System.out.println("Enter your x, y and z positions");
        	xPosition = scanner.nextDouble();
        	yPosition = scanner.nextDouble();
        	zPosition = scanner.nextDouble();
        	System.out.println("Enter your x, y and z velocities");
        	xVelocity = scanner.nextDouble();
        	yVelocity = scanner.nextDouble();
        	zVelocity = scanner.nextDouble();
        	System.out.println("Please enter your particle's mass.");  
        	Mass = scanner.nextDouble();
        	pass = true;
        	
        	                           
        	particles.add(new Particle(xVelocity,yVelocity,zVelocity,xPosition,yPosition,zPosition,Mass,Name));
        	
        }
    }
}