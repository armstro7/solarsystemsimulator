import java.util.*;
import java.io.*;
import java.text.*;
import java.util.concurrent.TimeUnit;
 
public class Simulation4
{
	/**
	*Here is the main method. It creates the array of particles and allows the 
	*user to input various parameters for the simulation, letting them choose 
	*between a preset and a custom system.
	*The paramaters available are: the number of seconds between simulated times
	*, the number of simulated seconds, the number of seconds between outputs,
	*and whether they are using the Euler-Cromer or Euler-Richardson methods.
	*As writing to many files is slow, but I feel the best option right now 
	*without access to Excel, there is an option to widen the time between 
	*outputs.
	*You can choose 
	*@author Sam Armstrong
	*@version Who even knows anymore, 4 apparently
	*/
    public static void main(String[] args) throws IOException
    {
        Scanner scanner = new Scanner(System.in);        
        String choice;
        ArrayList<Particle> particles = new ArrayList<Particle>();//Arraylist allows for a varying number of particles  
        Date startTime = Calendar.getInstance().getTime();
    	String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());
        
        System.out.println("Type Y to simulate the solar system, and N to simulate a custom system.");
        boolean started = false; //This looping section is for error catching
        while(started == false)
        {
        	choice = scanner.nextLine();
        	if(choice.equals("Y"))
        	{
        		Particle.SolarSystem(particles);
        		started = true;
        	}
        	else if(choice.equals("N"))
        	{
        		Particle.CustomSystem(particles);
        		started = true;
        	} 
        	else
        	{
        		System.out.println("Please enter one of the two options above.");
        	}
        }
        System.out.println("Please enter a step length and the length of the simulation in seconds.");
        double step = scanner.nextDouble();
        double simulationLength = scanner.nextDouble();
        System.out.println("Please enter how many steps there are between outputs.");
        double outputStep = scanner.nextDouble();
        System.out.println("Enter 1 to use the Euler-Cromer method, and 2 to use the Euler-Richardson method.");
        FileOperations.createDirectory(particles,timeStamp);
        String methodChoice = scanner.nextLine(); //Omnom newcharacter line. nextDouble should've eaten it first.
        methodChoice = scanner.nextLine();
        started = false;
        while(started == false)
        {
        	if(methodChoice.equals("1"))
        	{
        		runSimulation.EulerCromer(particles,step,simulationLength,timeStamp,outputStep,startTime);
        		started = true;
        	}
        	else if(methodChoice.equals("2"))
        	{
        		runSimulation.EulerRichardson(particles, step, simulationLength, timeStamp,outputStep, startTime);  
        		started = true;
        	} 
        	else
        	{
        		System.out.println("Please enter one of the two options above.");
        	}
        }
    }
}