import java.util.*;
import java.io.*;
import java.text.*;
import java.util.concurrent.TimeUnit;
 
public class runSimulation
{
	
    /**
    *Runs through the simulation using the Euler-Richardson method of numerical 
    *integration. It calculates the effects due to gravity from every other 
    *particle on the change in velocity of each particle before updating the 
    *positions of all of them for half a step. It then uses the acceleration at this point to 
    *calculate the final velocities and positions at the end of the step.
    *The positions can then be outputted to a file, and it would be trivial to 
    *add an option to output velocities as well.
    *@param particles The list containing all particles
    *@param step The step length for estimating velocity and position change during
    *@param simulationLength The total time the simulation will simulate passing
    *@param timeStamp The string containing the timeStamp at the start of running 
    *the program. For finding where to print files to.
    *@param outputStep How long to wait before printing out values
    *@param startTime The date where the program was started, for finding out how 
    *long the simulation ran for for diagnostic purposes.
    */
    public static void EulerRichardson(ArrayList<Particle> particles, double step,double simulationLength, String timeStamp, double outputStep, Date startTime) throws IOException
    {
    	int percentage = 0;    	
        double time = 0;
        int outputNumber = 0;
        double gEnergy = 0;
        double totalEnergy = 0;
        boolean pass = false;
        double initialTotalEnergy = 0;
        double energyDifference = 0;
        
    	while(time<simulationLength)
        {
        	
        	EulerCromerMini(particles,step);
        	for(int i = 0;i<particles.size();i++)
        	{
        		for(int j = 0; j<particles.size();j++)
        		{
        			if(i!=j)//Make sure a particle doesn't affect itself
        			{        
        				particles.get(i).gravField.updateMidGravField(particles.get(i).midPosition,particles.get(j).midPosition,particles.get(j).mass,step);
        				particles.get(i).updateRichardsonVelocity(particles.get(i).gravField,step);
        			}
        		}      
        		particles.get(i).gEnergy = gEnergy;
        		gEnergy = 0;//Wipe for next particle
        		particles.get(i).particleEnergy();
        	}
        	for(int i = 0;i<particles.size();i++)
        	{
        		particles.get(i).updateRichardsonPosition(step);    
        		totalEnergy+=particles.get(i).energy;//Sum the GPEs and KEs of every particle        		
        	}
       		if(pass == false)
       		{
       			initialTotalEnergy=totalEnergy;
       		}
       		
        	if(time>=outputNumber*outputStep)//For dropping the number of outputs since it's super slow
        	{
        		FileOperations.printEverything(particles, timeStamp, totalEnergy);
        		outputNumber++;
        	}
        	
        	time = time + step;  
        	if(time >= simulationLength*0.01*percentage) //It's nice to have some visual feedback of how far along the simulation is
        	{
        		System.out.println("Your simulation is " + percentage + "% complete");
        		percentage++;
        	}        	
        	if(time >= simulationLength)
        	{
        		energyDifference = initialTotalEnergy - totalEnergy; 
        	}
        	if(time<simulationLength)
        	{
        	   	totalEnergy = 0;
        	}
        	pass = true;
        }
        for(int i = 0;i<particles.size();i++) //Some immediate feedback, not particularly necessary but oh well.
        {
        	System.out.println(particles.get(i).name);
        	particles.get(i).position.print();
        	particles.get(i).velocity.print();
        }
        FileOperations.printDiagnostics(timeStamp, energyDifference, startTime, totalEnergy,step,simulationLength,outputStep);
    }
    
    /**
    *Runs through the simulation using the Euler-Cromer method of numerical 
    *integration. It calculates the effects due to gravity from every other 
    *particle on the change in velocity of each particle before updating the 
    *positions of all of them for half a step. This is for use in the Euler-
    *Richardson method above, for calculating the midpoints.
    *@param particles The list containing all particles
    *@param step The step length for estimating velocity and position change during
    *the program.
    */
    public static void EulerCromerMini(ArrayList<Particle> particles, double step)
    {
    	step = 0.5*step;
    	for(int i = 0;i<particles.size();i++)
     	{
     		for(int j = 0; j<particles.size();j++)
     		{
     			if(i!=j)//Make sure a particle doesn't affect itself
     			{        
     				particles.get(i).gravField.updateGravField(particles.get(i).position,particles.get(j).position,particles.get(j).mass,2*step);
     				
     				particles.get(i).updateMidVelocity(particles.get(i).gravField,step);
     			}
     		}      
     	}
     	for(int i = 0;i<particles.size();i++)
     	{
     		particles.get(i).updateMidPosition(step);      		
     	}
    }
    
    /**
    *Runs through the simulation using the Euler-Cromer method of numerical 
    *integration. It calculates the effects due to gravity from every other 
    *particle on the change in velocity of each particle before updating the 
    *positions of all of them.
    *The positions can then be outputted to a file, and it would be trivial to 
    *add an option to output velocities as well.
    *@param particles The list containing all particles
    *@param step The step length for estimating velocity and position change during
    *@param simulationLength The total time the simulation will simulate passing
    *@param timeStamp The string containing the timeStamp at the start of running 
    *the program. For finding where to print files to.
    *@param outputStep How long to wait before printing out values
    *@param startTime The date where the program was started, for finding out how 
    *long the simulation ran for for diagnostic purposes.
    */
    public static void EulerCromer(ArrayList<Particle> particles, double step,double simulationLength, String timeStamp, double outputStep, Date startTime) throws IOException
    {
    	int percentage = 0;    	
        double time = 0;
        int outputNumber = 0;
        double gEnergy = 0;
        double totalEnergy = 0;
        boolean pass = false;
        double initialTotalEnergy = 0;
        double energyDifference = 0;
        
    	while(time<simulationLength)
        {
        	for(int i = 0;i<particles.size();i++)
        	{
        		for(int j = 0; j<particles.size();j++)
        		{
        			if(i!=j)//Make sure a particle doesn't affect itself
        			{        
        				particles.get(i).gravField.updateGravField(particles.get(i).position,particles.get(j).position,particles.get(j).mass,step);
        				gEnergy += particles.get(i).gravField.particleGravitationalPotentialEnergy(step,particles.get(i).mass);//Sum all GPEs
        				particles.get(i).updateVelocity(particles.get(i).gravField,step);
        			}
        		}      
        		particles.get(i).gEnergy = gEnergy;
        		gEnergy = 0;//Wipe for next particle
        		particles.get(i).particleEnergy();
        	}
        	for(int i = 0;i<particles.size();i++)
        	{
        		particles.get(i).updatePosition(step);    
        		totalEnergy+=particles.get(i).energy;//Sum the GPEs and KEs of every particle        		
        	}
        	if(pass == false)
       		{
        		initialTotalEnergy=totalEnergy;
       		}
        	
        	if(time>=outputNumber*outputStep)//For dropping the number of outputs since it's super slow
        	{
        		FileOperations.printEverything(particles, timeStamp, totalEnergy);
        		outputNumber++;
        	}
        	
        	time = time + step;  
        	if(time >= simulationLength*0.01*percentage) //It's nice to have some visual feedback of how far along the simulation is
        	{
        		System.out.println("Your simulation is " + percentage + "% complete");
        		percentage++;
        	}        	
        	if(time >= simulationLength)
        	{
        		energyDifference = initialTotalEnergy - totalEnergy; 
        	}
        	if(time<simulationLength)
        	{
        	   	totalEnergy = 0;
        	}
        	pass = true;
        }
        for(int i = 0;i<particles.size();i++) //Some immediate feedback, not particularly necessary but oh well.
        {
        	System.out.println(particles.get(i).name);
        	particles.get(i).position.print();
        	particles.get(i).velocity.print();
        }
        FileOperations.printDiagnostics(timeStamp, energyDifference, startTime, totalEnergy,step,simulationLength,outputStep);
    }
}