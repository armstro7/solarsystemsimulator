Welcome to my gravity simulator for particle systems.

You will need to compile the program by navigating to this folder in commandline and running "javac Simulation4.java", then "java Simulation4"

To start with, you will need to choose whether you will be starting using the initial conditions already programmed in, in this case being the solar system from the 30th of October at midnight, or input your own particle system.
Press Y to simulate the solar system, or N to run your own simulation.

Solar System:
If you have selected this, you will start by entering a step length in seconds. This is the length of time that will pass between the simulated times in the system

Next, you will enter the length of time that the simulation will simulate in seconds. Note: There are approximately 3.16E7 seconds in a year.

Next, you will enter the number of steps that will be calculated between outputs. I recommend having this be as large as possible while still getting your data, as the printing method is rather time-consuming.

Finally, you will choose whether to use the Euler-Cromer or Euler-Richardson method. The Richardson method takes longer but is more accurate. 

Custom Particle System:
If you have selected this, you will need to enter every particle in individually for the current build. 

Initially your choice does not matter as you will be forced to enter at least one particle.

You will next have to enter a name followed by the x, y and z positions in metres, the x, y and z velocities in metres per second, and the mass of the particle you wish to add to the simulation. Please enter the positions, velocities and mass as double values, it's not set to deal with incorrect entries.

After at least one particle is entered, you may enter "N" to exit and begin the simulation, but it is recommended to simulate more than one as otherwise it will just sit on its own doing nothing.

Next, you will enter the length of time that the simulation will simulate in seconds. Note: There are approximately 3.16E7 seconds in a year.

Next, you will enter the number of steps that will be calculated between outputs. I recommend having this be as large as possible while still getting your data, as the printing method is rather time-consuming.

Finally, you will choose whether to use the Euler-Cromer or Euler-Richardson method. The Richardson method takes longer but is more accurate.

Outputs: 
This program will create a folder based on the time you started the simulation, and inside it will create a new set of folders based on the names of your particles. Inside these will be separate txt files containing all of the printed values, with separate files for x positions, y positions, z velocities, etc. This may seem rather inefficient, but it is, in my opinion, the best way to split txt files to be able to quickly input into graphic systems for now.